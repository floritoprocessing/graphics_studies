package graf.simterrain;

import processing.core.PApplet;
import processing.core.PImage;

public class Terrain {

	private final PApplet pa;
	
	private final int width, height;
	private double[] terrain;
	private PImage asImage = null;
	
	public Terrain(PApplet pa, int width, int height) {
		this.pa = pa;
		this.width = width;
		this.height = height;
		terrain = new double[width*height];
	}
	
	public void addNoise(double amp, float detail) {
		addNoise(amp,detail,detail);
	}
	
	public void addNoise(double amp, float xDetail, float yDetail) {
		float seed = (float)(1000*Math.random());
		int i=0;
		xDetail/=width;
		yDetail/=height;
		for (int y=0;y<height;y++) {
			for (int x=0;x<width;x++) {
				terrain[i] += amp*pa.noise(x*xDetail,y*yDetail,seed);
				i++;
			}
		}
	}
	
	public void clear() {
		for (int i=0;i<terrain.length;i++) terrain[i]=0;
	}

	public PImage getAsImage() {
		return asImage;
	}

	public void multiply(double factor) {
		Procfuncs.multiply(terrain, factor);
	}

	public void normalize() {
		Procfuncs.normalize(terrain);
	}

	public void renderAsImage() {
		asImage = new PImage(width,height);
		asImage.loadPixels();
		int v=0;
		double[] val = new double[terrain.length];
		System.arraycopy(terrain, 0, val, 0, terrain.length);
		Procfuncs.normalize(val);
		Procfuncs.multiply(val, 255);
		for (int i=0;i<terrain.length;i++) {
			v = (int)Math.max(0, Math.min(255,val[i]));
			asImage.pixels[i] = 0xff<<24 | v<<16 | v<<8 | v;
		}
		asImage.updatePixels();
	}

	public void threshold(double threshold, double min, double max) {
		Procfuncs.threshold(terrain, threshold, min, max);
	}

	public void threshold(double threshold, double crossOverRange, double min, double max) {
		Procfuncs.threshold(terrain, threshold, crossOverRange, min, max);
	}

	public void multiply(Terrain grad) {
		if (grad.width==width && grad.height==height) {
			for (int i=0;i<terrain.length;i++) {
				terrain[i] *= grad.terrain[i];
			}
		} else {
			throw new RuntimeException("Cannot multiply terrains with different dimensions");
		}
	}

	public double[] getTerrainData() {
		return terrain;
	}

}
