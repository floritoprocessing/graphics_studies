package graf.simterrain;

import java.io.IOException;

import processing.core.*;

public class SimTerrain extends PApplet {

	private static final long serialVersionUID = -5861746977913460829L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.simterrain.SimTerrain"});
	}
	
	private int terrainWidth = 200; //200
	private int terrainHeight = 150; //150
	private float terrainScale = 1;//0.25f; //1 
	
	private Terrain terrain;
	
	private float dirLightX= 0.25390625f;
	private float dirLightY= 0.3932292f;
	private float dirLightZ=-1;
	
	private float lateralShiftPercentage = 0.05f;
	private int lateralPixelOffset = 50;
	
	private static final int LEFT = 1;
	private static final int RIGHT = 2;
	
	private PImage left, right;
	
	public void settings() {
		size(1024,768,P3D);
	}
	public void setup() {
		
		initTerrain();
		smooth();
		left = new PImage(width,height);
		right = new PImage(width,height);
		left.loadPixels();
		right.loadPixels();
	}
	
	private void initTerrain() {
		frameCount=0;
		terrain = new Terrain(this,terrainWidth,terrainHeight);
		
		// plateaus
		terrain.addNoise(1.0, 25);
		
		Terrain grad = new Terrain(this,terrainWidth,terrainHeight);
		grad.addNoise(1.0,2);
		grad.normalize();
		
		terrain.multiply(grad);
		terrain.threshold(0.45, 0.1, 0, 1);
		
		terrain.addNoise(0.2, 2);
		terrain.addNoise(0.5, 10);
		
		terrain.normalize();
		terrain.multiply(2.5);
		
		//terrain.addNoise(2.0f, 5, 5);
		//terrain.addNoise(4.0f, 2, 2);
		//terrain.normalize();
		//terrain.multiply(255);
		terrain.renderAsImage();
	}
	
	public void mousePressed() {
		if (mouseButton==LEFT) initTerrain();
	}
	
	public void draw() {
		
		background(0);
		//pushMatrix();
		//image(terrain.getAsImage(),0,0,width,height);
		//popMatrix();
		
		int i=0;
		float z0,z1,z2,z3;
		double[] data = terrain.getTerrainData();
		
		noFill();
		
		//beginCamera();
		float totWidth = terrainWidth*terrainScale;
		float lateralShift = (frameCount%2==1?-1:1)*(lateralShiftPercentage*totWidth*0.5f);
		if (frameCount!=LEFT && frameCount!=RIGHT) lateralShift=0;
		camera(totWidth/2 + lateralShift,terrainHeight*terrainScale/2,150,
				totWidth/2  + lateralShift,terrainHeight*terrainScale/2,0,
				0,1,0);
		//perspective(15 *PI/180.0f, (float)width/height, -2000, 5000);
		
		
		// show terrain as image
		/*
		noStroke();
		fill(255);
		beginShape(PApplet.QUAD);
		textureMode(PApplet.NORMAL);
		texture(terrain.getAsImage());
		vertex(0,0,-100,0,0);
		vertex(terrainWidth*terrainScale,0,-100,1,0);
		vertex(terrainWidth*terrainScale,terrainHeight*terrainScale,-100,1,1);
		vertex(0,terrainHeight*terrainScale,-100,0,1);
		endShape();
		*/
		
		
		//lights();
		//ambientLight(102, 102, 102);
		if (mousePressed && mouseButton==RIGHT) {
			dirLightX = - (2*(float)mouseX/width-1);
			dirLightY = - (2*(float)mouseY/height-1);
			System.out.println(dirLightX+" "+dirLightY);
		}
		directionalLight(200,200,200, dirLightX, dirLightY, dirLightZ);
		fill(255,255,255);
		noStroke();
		beginShape(PApplet.QUADS);
		float x0,x1, y0,y1;
		for (int y=0;y<terrainHeight-1;y++) {
			for (int x=0;x<terrainWidth-1;x++) {
				x0 = x*terrainScale;
				x1 = (x+1)*terrainScale;
				y0 = y*terrainScale;
				y1 = (y+1)*terrainScale;
				z0 = (float)data[i];
				z1 = (float)data[i+1];
				z2 = (float)data[i+1+terrainWidth];
				z3 = (float)data[i+terrainWidth];
				vertex(x0,y0,z0);
				vertex(x1,y0,z1);
				vertex(x1,y1,z2);
				vertex(x0,y1,z3);
				i++;
			}
			i++;
		}
		endShape();
		
		if (frameCount==LEFT) {
			//loadPixels();
			loadPixels();
			System.arraycopy(pixels, 0, left.pixels, 0, pixels.length);
			left.updatePixels();
			left.save(sketchPath("SimTerrainLeft.jpg"));
			//left.co
		} else if (frameCount==RIGHT) {
			loadPixels();
			System.arraycopy(pixels, 0, right.pixels, 0, pixels.length);
			right.updatePixels();
			right.save(sketchPath("SimTerrainRight.jpg"));
			
			PImage lr = new PImage(width,height);
			int l, r;
			int p = 0;
			for (int y=0;y<height;y++) {
				for (int x=0;x<width;x++) {
					l = left.pixels[p]&0xff;
					if (x-lateralPixelOffset>0)
						r = right.pixels[p-lateralPixelOffset]&0xff;
					else
						r = 0;
					lr.pixels[p] = 0xff<<24 | l<<16 | r<<8 | r;
					p++;
				}
			}
			lr.updatePixels();
			lr.save(sketchPath("SimTerrain.jpg"));
			
//			try {
//				Runtime.getRuntime().exec("cmd /k start c:/SimTerrain.tif");
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
			exit();
//			stop();
//			System.exit(0);
		}
	}

}
