package graf.simterrain;

public class Procfuncs {

	public static void multiply(double[] t, double factor) {
		for (int i=0;i<t.length;i++) {
			t[i] *= factor;
		}
	}

	public static void normalize(double[] t) {
		double min=Double.MAX_VALUE;
		double max=Double.MIN_VALUE;
		for (int i=0;i<t.length;i++) {
			min = Math.min(min, t[i]);
			max = Math.max(max, t[i]);
		}
		double dx = max-min;
		for (int i=0;i<t.length;i++) {
			t[i] = (t[i]-min)/dx;
		}
	}
	
	public static void threshold(double[] t, double threshold, double min, double max) {
		for (int i=0;i<t.length;i++) {
			t[i] = (t[i]<threshold)?min:max;
		}
	}

	public static void threshold(double[] t, double threshold, double crossOverRange, double min, double max) {
		double t0 = threshold-crossOverRange/2.0;
		double t1 = threshold+crossOverRange/2.0;
		
		for (int i=0;i<t.length;i++) {
			if (t[i]<t0) {
				t[i] = min;
			} else if (t[i]<t1) {
				t[i] = min + (t[i]-t0)/crossOverRange * (max-min);
			} else {
				t[i] = max;
			}
		}
	}
	
}
