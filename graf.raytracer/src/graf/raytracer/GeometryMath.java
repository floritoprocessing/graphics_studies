package graf.raytracer;

import graf.math.matrix.Matrix;

public class GeometryMath {

	/**
	 * Returns the intersection point of the inifinite plane and line
	 * @see http://mathworld.wolfram.com/Line-PlaneIntersection.html
	 * @param plane
	 * @param line
	 * @return
	 */
	public static Vec planeLineIntersection(Plane plane, Line line) {
		double x1 = plane.getX0();
		double x2 = plane.getX1();
		double x3 = plane.getX2();
		double y1 = plane.getY0();
		double y2 = plane.getY1();
		double y3 = plane.getY2();
		double z1 = plane.getZ0();
		double z2 = plane.getZ1();
		double z3 = plane.getZ2();
		
		double x4 = line.getA().getX();
		double x5 = line.getB().getX();
		double y4 = line.getA().getY();
		double y5 = line.getB().getY();
		double z4 = line.getA().getZ();
		double z5 = line.getB().getZ();
		
		double det1 = new Matrix(new double[][] { {1,1,1,1}, {x1,x2,x3,x4}, {y1,y2,y3,y4}, {z1,z2,z3,z4} }).det();
		double det2 = new Matrix(new double[][] { {1,1,1,0}, {x1,x2,x3,x5-x4}, {y1,y2,y3,y5-y4}, {z1,z2,z3,z5-z4} }).det();
		
		double t = -det1/det2;
		
		double x = x4 + t*(x5-x4);
		double y = y4 + t*(y5-y4);
		double z = z4 + t*(z5-z4);
		
		return new Vec(x,y,z);
	}
	
	/**
	 * Returns the intersection point of the finite <i>plane</i> and infinite <i>line</i><br>
	 * Returns null if the finite plane does not intersect
	 * @param surface
	 * @param line
	 * @return
	 */
	public static Vec surfaceLineIntersection(Plane surface, Line line) {
		Vec intersect = planeLineIntersection(surface, line);
		if (pointInTriangle(intersect, surface))
			return intersect;
		else
			return null;
	}
	
	/**
	 * http://www.blackpawn.com/texts/pointinpoly/default.html
	 * @param point
	 * @param triangle
	 * @return
	 */
	public static boolean pointInTriangle(Vec P, Plane triangle) {
		Vec A = triangle.getA();
		Vec B = triangle.getB();
		Vec C = triangle.getC();

		// Compute vectors   
		Vec v0 = Vec.sub(C,A);
		Vec v1 = Vec.sub(B,A);
		Vec v2 = Vec.sub(P,A);
		
		// Compute dot products
		double dot00 = Vec.dot(v0, v0);
		double dot01 = Vec.dot(v0, v1);
		double dot02 = Vec.dot(v0, v2);
		double dot11 = Vec.dot(v1, v1);
		double dot12 = Vec.dot(v1, v2);
		
		// Compute barycentric coordinates
		double invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
		double u = (dot11 * dot02 - dot01 * dot12) * invDenom;
		double v = (dot00 * dot12 - dot01 * dot02) * invDenom;
		
		// Check if point is in triangle
		return (u > 0) && (v > 0) && (u + v < 1);
	}
	
}
