package graf.raytracer;

public class Vec implements Cloneable {
	
	public static Vec add(Vec a, Vec b) {
		return new Vec(a.x+b.x,a.y+b.y,a.z+b.z);
	}
	
	/**
	 * http://www.analyzemath.com/vector_calculators/vector_cross_product.html
	 * @param u
	 * @param v
	 * @return
	 */
	public static Vec crossProduct(Vec u, Vec v) {
		//u = < a , b , c > and v = < d , e , f >
		//x = b*f - c*e , y = c*d - a*f and z = a*e - b*d 
		double a=u.x, b=u.y, c=u.z;
		double d=v.x, e=v.y, f=v.z;
		return new Vec(b*f - c*e, c*d - a*f, a*e - b*d);
	}
	
	public static Vec divide(Vec a, double fac) {
		return new Vec(a.x/fac,a.y/fac,a.z/fac);
	}

	public static double dot(Vec a, Vec b) {
		return a.x*b.x + a.y*b.y + a.z*b.z;
	}
	
	public static Vec mirror(Vec sourceVec, Vec targetVec) {
		Vec u = Vec.multiply(targetVec,2*Vec.dot(sourceVec, targetVec)/targetVec.getLengthSquared());
		Vec mirrored = (Vec)sourceVec.clone();
		mirrored.subtract(u);
		return mirrored;
	}

	public static Vec multiply(Vec a, double fac) {
		return new Vec(a.x*fac,a.y*fac,a.z*fac);
	}

	public static Vec random(double rangeMin, double rangeMax) {
		double x = rangeMin + Math.random()*(rangeMax-rangeMin);
		double y = rangeMin + Math.random()*(rangeMax-rangeMin);
		double z = rangeMin + Math.random()*(rangeMax-rangeMin);
		return new Vec(x,y,z);
	}

	public static Vec random(double xMin, double xMax, double yMin, double yMax, double zMin, double zMax) {
		double x = xMin + Math.random()*(xMax-xMin);
		double y = yMin + Math.random()*(yMax-yMin);
		double z = zMin + Math.random()*(zMax-zMin);
		return new Vec(x,y,z);
	}
	
	public static Vec sub(Vec a, Vec b) {
		return new Vec(a.x-b.x,a.y-b.y,a.z-b.z);
	}

	private double x=0, y=0, z=0;
	
	public Vec() {
	}

	public Vec(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec(Vec a) {
		x=a.x;
		y=a.y;
		z=a.z;
	}

	public void add(double xa, double ya, double za) {
		x += xa;
		y += ya;
		z += za;
	}

	public void add(Vec p) {
		x += p.x;
		y += p.y;
		z += p.z;
	}
	
	public Object clone() {
		Vec out = new Vec(this.x,this.y,this.z);
		return out;
	}
	
	public void divide(double fac) {
		x/=fac;
		y/=fac;
		z/=fac;
	}

	public double getLength() {
		if (isNull())
			return 0;
		else
			return Math.sqrt(x*x+y*y+z*z);
	}
	
	public double getLengthSquared() {
		if (isNull())
			return 0;
		else
			return x*x+y*y+z*z;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public boolean isNull() {
		return (x==0&&y==0&&z==0);
	}
	
	public void multiply(double fac) {
		x*=fac;
		y*=fac;
		z*=fac;
	}
	
	public void normalize() {
		divide(getLength());
	}
	
	public void setLength(double len) {
		normalize();
		multiply(len);
	}
	
	public void setNull() {
		x=0;
		y=0;
		z=0;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
	public void subtract(double xa, double ya, double za) {
		x -= xa;
		y -= ya;
		z -= za;
	}

	public void subtract(Vec p) {
		x -= p.x;
		y -= p.y;
		z -= p.z;
	}
	
	
}
