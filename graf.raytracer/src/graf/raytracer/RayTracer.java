package graf.raytracer;

import java.util.Vector;

import graf.math.matrix.Matrix;
import processing.core.PApplet;
import processing.core.PImage;

public class RayTracer extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.raytracer.RayTracer"}); // arg0: "graf.raytracer.RayTracer"
	}

	Vec direction;
	Vector<Vec> rayPoints = new Vector<Vec>();
	Vector<Plane> planes = new Vector<Plane>();
	int MAX_REFLECT = 5;
	PImage image3d;
	
	private int downX=0;
	private double offsetX = 0, offsetXadd = 0;

	public void settings() {
		size(600,400,P3D);
	}
	
	public void setup() {
		image3d = new PImage(600,400);
		background(0);
		newTriangles();
		newRay();
	}

	public void newTriangles() {
		planes.clear();
		for (int i=0;i<10;i++) {
			double size = 150;
			Vec a = Vec.random(-200,200,-200,200,-300,-500);
			Vec b = Vec.add(a,Vec.random(-size,size,-size,size,-size,size));
			Vec c = Vec.add(a,Vec.random(-size,size,-size,size,-size,size));
			planes.add(new Plane(a,b,c));
		}
		planes.add(new Plane(new Vec(-200,-200,-100),new Vec(-200,-200,-500),new Vec(-200,200,-500)));
		planes.add(new Plane(new Vec(-200,-200,-100),new Vec(-200,200,-100),new Vec(-200,200,-500)));
	}
	
	public void newRay() {
		newRay(-200+Math.random()*400,-200+Math.random()*400);
	}

	public void newRay(double x, double y) {
		Vec startPoint = new Vec(x,y,-100);
		direction = new Vec(0,0,-150);

		rayPoints.clear();
		rayPoints.add(startPoint);

		int reflections = 0;
		while (reflections<MAX_REFLECT && reflections>=0) {
		
			/*
			 * Get closest intersection between last rayPoint and rayPoint+direction
			 */
			Vec closestIntersection = null;
			Plane intersectedPlane = null;
			double closestIntersectionDistance = Double.MAX_VALUE;
			for (int i=0;i<planes.size();i++) {
				Line ray = new Line(rayPoints.elementAt(rayPoints.size()-1),Vec.add(startPoint,direction));
				Vec intersection = GeometryMath.surfaceLineIntersection(planes.elementAt(i), ray);
				if (intersection!=null) {
					double distance = Vec.sub(direction,startPoint).getLength();
					if (distance<closestIntersectionDistance) {
						closestIntersectionDistance=distance;
						closestIntersection = intersection;
						intersectedPlane = planes.elementAt(i);
					}
				}
			}
	
			if (closestIntersection!=null) {
				Vec normal = intersectedPlane.getNormal();
				direction = Vec.mirror(direction, normal);
				rayPoints.add((Vec)closestIntersection.clone());
				reflections++;
			} else {
				reflections = -1;
			}
		
		}
		
		if (reflections>0) {
			rayPoints.add(Vec.add(rayPoints.elementAt(rayPoints.size()-1),direction));
		}

	}

	public void keyPressed() {
		newTriangles();
		newRay();
	}
	
	public void mousePressed() {
		if (mouseButton==RIGHT) {
			downX = mouseX;
		}
	}
	
	public void mouseReleased() {
		if (mouseButton==RIGHT) {
			offsetX += offsetXadd;
			offsetXadd = 0;
		}
	}



	public void draw() {
		if (mousePressed) {
			if (mouseButton==LEFT)
				newRay(mouseX-width/2,mouseY-height/2);
			else if (mouseButton==RIGHT)
				offsetXadd = (downX-mouseX)*0.5;
		}
		
		for (int eye=-1;eye<=1;eye+=2) {
		
			background(0);
			stroke(255);
			noFill();
	
			pushMatrix();
			translate(width/2-eye*20 + (float)(offsetX + offsetXadd),height/2);
			//rotateY(HALF_PI/4*sin(frameCount/50.0f));
			//rotateX(HALF_PI/16*cos(frameCount/40.0f));
	
			rectMode(CORNERS);
			stroke(255);
			noFill();
			pushMatrix();
			translate(0,0,-100);
			rect(-200,-200,200,200);
			popMatrix();
	
			stroke(255,255,255,128);
			fill(255,255,255,64);
			for (int i=0;i<planes.size();i++) {
				beginShape(TRIANGLES);
				vertex((float)planes.elementAt(i).getX0(),(float)planes.elementAt(i).getY0(),(float)planes.elementAt(i).getZ0());
				vertex((float)planes.elementAt(i).getX1(),(float)planes.elementAt(i).getY1(),(float)planes.elementAt(i).getZ1());
				vertex((float)planes.elementAt(i).getX2(),(float)planes.elementAt(i).getY2(),(float)planes.elementAt(i).getZ2());
				endShape();
			}
	
			
			if (rayPoints.size()>=2) {
				stroke(255);
				beginShape(LINES);
				for (int i=0;i<rayPoints.size()-1;i++) {
					double x0 = rayPoints.elementAt(i).getX();
					double y0 = rayPoints.elementAt(i).getY();
					double z0 = rayPoints.elementAt(i).getZ();
					double x1 = rayPoints.elementAt(i+1).getX();
					double y1 = rayPoints.elementAt(i+1).getY();
					double z1 = rayPoints.elementAt(i+1).getZ();
					vertex((float)x0,(float)y0,(float)z0);
					vertex((float)x1,(float)y1,(float)z1);
				}
				endShape();
			} else {
				stroke(255,255,0,64);
				beginShape(LINES);
				double x0 = rayPoints.elementAt(0).getX();
				double y0 = rayPoints.elementAt(0).getY();
				double z0 = rayPoints.elementAt(0).getZ();
				double x1 = x0 + direction.getX();
				double y1 = y0 + direction.getY();
				double z1 = z0 + direction.getZ();
				vertex((float)x0,(float)y0,(float)z0);
				vertex((float)x1,(float)y1,(float)z1);
				endShape();
			}
			popMatrix();
			
			loadPixels();
			if (eye==-1) {
				for (int i=0;i<pixels.length;i++) {
					int br = ((pixels[i]>>16&0xff) + (pixels[i]>>8&0xff) + (pixels[i]&0xff))/3;
					image3d.pixels[i] = 0xff<<24 | br<<16 | br<<8 | br;
				}
			} else {
				for (int i=0;i<pixels.length;i++) {
					int br = ((pixels[i]>>16&0xff) + (pixels[i]>>8&0xff) + (pixels[i]&0xff))/3;
					int r = (image3d.pixels[i]>>16&0xff);
					image3d.pixels[i] = 0xff<<24 | r<<16 | br<<8 | br;
				}
			}
			
			image(image3d,0,0);
		
		}

	}

}
