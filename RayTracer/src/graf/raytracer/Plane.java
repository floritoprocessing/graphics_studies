package graf.raytracer;

public class Plane implements Cloneable {

	private Vec center=null, normal=null;
	private final Vec a,b,c;
	
	public Plane(double x1, double y1, double z1, 
			double x2, double y2, double z2,
			double x3, double y3, double z3) {
		
		a = new Vec(x1,y1,z1);
		b = new Vec(x2,y2,z2);
		c = new Vec(x3,y3,z3);
	}
	
	public Plane(Vec a, Vec b, Vec c) {
		this.a = (Vec)a.clone();
		this.b = (Vec)b.clone();
		this.c = (Vec)c.clone();
	}

	public Object clone() {
		return new Plane((Vec)a.clone(),(Vec)b.clone(),(Vec)c.clone());
	}
	
	public double getX0() {
		return a.getX();
	}
	public double getX1() {
		return b.getX();
	}
	public double getX2() {
		return c.getX();
	}
	public double getY0() {
		return a.getY();
	}
	public double getY1() {
		return b.getY();
	}
	public double getY2() {
		return c.getY();
	}
	public double getZ0() {
		return a.getZ();
	}
	public double getZ1() {
		return b.getZ();
	}
	public double getZ2() {
		return c.getZ();
	}

	public Vec getCenter() {
		if (center==null) center = Vec.divide(Vec.add(a,Vec.add(b,c)),3);
		return center;
	}

	public Vec getNormal() {
		if (normal==null) normal = Vec.crossProduct(Vec.sub(b, a), Vec.sub(c,a));
		return normal;
	}
	
	public void scale(double fac) {
		getCenter();
		
		a.add(center);
		b.add(center);
		c.add(center);
		
		a.multiply(fac);
		b.multiply(fac);
		c.multiply(fac);
		
		center.multiply(-1);
		a.add(center);
		b.add(center);
		c.add(center);
	}

	/**
	 * Returns a a clone of corner point A
	 * @return
	 */
	public Vec getA() {
		return new Vec(a);
	}
	/**
	 * Returns a a clone of corner point B
	 * @return
	 */
	public Vec getB() {
		return new Vec(b);
	}
	/**
	 * Returns a a clone of corner point C
	 * @return
	 */
	public Vec getC() {
		return new Vec(c);
	}


}
