package graf.raytracer;

public class Line implements Cloneable {
	
	private Vec a, b;
	
	public Line(Vec a, Vec b) {
		this.a = (Vec)a.clone();
		this.b = (Vec)b.clone();
	}
	
	public Line(double x1, double y1, double z1, double x2, double y2, double z2) {
		a = new Vec(x1,y1,z1);
		b = new Vec(x2,y2,z2);
	}

	public Vec getA() {
		return a;
	}

	public void setA(Vec a) {
		this.a = (Vec)a.clone();
	}

	public Vec getB() {
		return b;
	}

	public void setB(Vec b) {
		this.b = (Vec)b.clone();
	}
	
	public void scale(double fac) {
		Vec center = (Vec)a.clone();
		center.add(b);
		center.multiply(-0.5);
		
		a.add(center);
		b.add(center);
		a.multiply(fac);
		b.multiply(fac);
		center.multiply(-1);
		a.add(center);
		b.add(center);
	}
	
	public Object clone() {
		return new Line((Vec)a.clone(),(Vec)b.clone());
	}
}
